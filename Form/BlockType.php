<?php

namespace Trendix\CmsBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Trendix\AdminBundle\Form\ColorType;
use Trendix\AdminBundle\Form\GalleryType;
use Trendix\AdminBundle\Form\HtmlType;
use Trendix\AdminBundle\Form\ImageType;
use Trendix\CmsBundle\Entity\Block;
use Trendix\CmsBundle\Entity\ColorBlock;

class BlockType extends AbstractType
{
    private $subBlock = false;

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if(isset($options['subBlock'])) {
            $this->subBlock = $options['subBlock'];
        }

        $types = array(
            '---' => '',
            'Bloque de texto' => 'text',
            'Bloque de texto e imagen' => 't_image'
        );
        $customBlocks = explode('|', getenv('CMS_BLOCKS'));
        $custom = [];
        foreach($customBlocks as $block) {
            $blockInfo = explode('-', $block);
            $custom[$blockInfo[1]] = $blockInfo[0];
        }
        if(count($custom)) {
            $types['Bloque especial'] = 'custom';
        }

        if(!$this->subBlock) {
            $types['Grupo de bloques en pestañas'] = 'tabs';
            $types['Fila de bloques de colores'] = 'colors';
            $types['Llamada a la acción'] = 'cta';
            $types['Grupo de bloques de iconos'] = 'icons';
            $types['Opiniones'] = 'testimonials';
            $types['Lista de checks'] = 'checklist';
        }

        $builder
            ->add('title', TextType::class, array('label' => 'Título', 'required' => true))
            ->add('customType', ChoiceType::class, array(
                'label' => 'Subtipo', 'required' => false,
                'choices' => $custom
            ))
            ->add('position', NumberType::class, array('label' => 'Posición', 'required' => true))
            ->add('type', ChoiceType::class, array(
                'label' => '¿Qué tipo de bloque quieres?',
                'required' => true,
                'choices' => $types,
                'attr' => array(
                    'class' => 'blocktype'
                )
            ))
            ->add('text', HtmlType::class, array('label' => 'Texto', 'required' => false))
            ->add('galleryLeft', choiceType::class, array(
                'label' => '¿Quieres mostrar la imagen a la izquierda o la derecha?',
                'choices' => array(
                    'Izquierda' => true,
                    'Derecha' => false
                )
            ))
            ->add('gallery', GalleryType::class, array('label' => 'Galería'))
            ->add('image', ImageType::class, array('label' => 'Imagen', 'required' => false))
            ->add('blocksPerRow', NumberType::class, array(
                'label' => 'Número de bloques por fila (tendrás que guardar el proyecto para poder introducir bloques)'
            ))
            ->add('link', TextType::class, array('label' => 'Enlace'))
            ->add('linkText', TextType::class, array('label' => 'Texto enlace'))
            ->add('ctaType', ChoiceType::class, array(
                'label' => 'Selecciona el tipo de llamada a la acción',
                'choices' => array(
                    'Texto y acción a la izquierda' => 'all_left',
                    'Texto a la izquierda, acción a la derecha' => 'left_right'
                )
            ))
            ->add('color', ColorType::class, array('label' => 'Color de texto (opcional)', 'required' => false))
            ->add('backgroundColor', ColorType::class, array('label' => 'Color de fondo (si no se selecciona imagen)', 'required' => false))
            ->add('withLinks', CheckboxType::class, array('label' => 'Bloques con texto y enlace', 'required' => false))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('subBlock', false);
    }

    public function getName()
    {
        return 'trendix_cms_block';
    }
}
<?php

namespace Trendix\CmsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Trendix\AdminBundle\Form\ColorType;

class ColorBlockType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, array('label' => 'Título'))
            ->add('position', NumberType::class, array('label' => 'Posición'))
            ->add('text', TextType::class, array('label' => 'Texto'))
            ->add('link', TextType::class, array('label' => 'Enlace', 'required' => false))
            ->add('color', ColorType::class, array('label' => 'Color de texto'))
            ->add('background', ColorType::class, array('label' => 'Color de fondo'))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Trendix\CmsBundle\Entity\ColorBlock'
        ));
    }
}
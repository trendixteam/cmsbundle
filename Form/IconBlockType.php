<?php

namespace Trendix\CmsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Trendix\AdminBundle\Form\ColorType;

class IconBlockType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, array('label' => 'Título'))
            ->add('position', NumberType::class, array('label' => 'Posición'))
            ->add('text', TextType::class, array('label' => 'Texto', 'required' => false))
            ->add('link', TextType::class, array('label' => 'Enlace', 'required' => false))
            ->add('linkText', TextType::class, array('label' => 'Texto enlace', 'required' => false))
            ->add('color', ColorType::class, array('label' => 'Color de texto', 'required' => false))
            ->add('icon_file', FileType::class,array(
                "label" => "Icono:",
                "attr" =>array("class" => "form-control"),
                "data_class" => null,
                "mapped" => false,
            ))

        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Trendix\CmsBundle\Entity\IconBlock'
        ));
    }
}
<?php

namespace Trendix\CmsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Trendix\AdminBundle\Form\ColorType;
use Trendix\AdminBundle\Form\GalleryType;
use Trendix\AdminBundle\Form\HtmlType;
use Trendix\AdminBundle\Form\ImageType;

class CallToActionBlockType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, array('label' => 'Título'))
            ->add('link', TextType::class, array('label' => 'Enlace'))
            ->add('linkText', TextType::class, array('label' => 'Texto enlace'))
            ->add('text', HtmlType::class, array('label' => 'Contenido'))
            ->add('ctaType', ChoiceType::class, array(
                'label' => 'Selecciona el tipo de llamada a la acción',
                'choices' => array(
                    'Texto y acción a la izquierda' => 'all_left',
                    'Texto a la izquierda, acción a la derecha' => 'left_right'
                )
            ))
            ->add('image', ImageType::class, array('label' => 'Imagen (opcional)'))
            ->add('position', NumberType::class, array('label' => 'Posición'))
            ->add('color', ColorType::class, array('label' => 'Color de texto (opcional)'))
            ->add('backgroundColor', ColorType::class, array('label' => 'Color de fondo (si no se selecciona imagen)'))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Trendix\CmsBundle\Entity\CallToActionBlock'
        ));
    }
}
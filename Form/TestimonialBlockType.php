<?php

namespace Trendix\CmsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Trendix\AdminBundle\Form\ColorType;
use Trendix\AdminBundle\Form\ImageType;

class TestimonialBlockType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('author', TextType::class, array('label' => 'Autor'))
            ->add('position', NumberType::class, array('label' => 'Posición'))
            ->add('text', TextType::class, array('label' => 'Texto'))
            ->add('place', TextType::class, array('label' => 'Lugar'))
            ->add('image', ImageType::class, array('label' => 'Foto'))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Trendix\CmsBundle\Entity\TestimonialBlock'
        ));
    }
}
<?php

namespace Trendix\CmsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Trendix\AdminBundle\Form\ColorType;
use Trendix\AdminBundle\Form\GalleryType;
use Trendix\AdminBundle\Form\HtmlType;
use Trendix\AdminBundle\Form\ImageType;

class TextImageBlockType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, array('label' => 'Título'))
            ->add('text', HtmlType::class, array('label' => 'Contenido'))
            ->add('galleryLeft', ChoiceType::class, array(
                'label' => '¿Quieres mostrar la imagen a la izquierda o la derecha?',
                'choices' => array(
                    'Izquierda' => true,
                    'Derecha' => false
                )
            ))
            ->add('image', ImageType::class, array('label' => 'Imagen'))
            ->add('position', NumberType::class, array('label' => 'Posición'))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Trendix\CmsBundle\Entity\TextImageBlock'
        ));
    }
}
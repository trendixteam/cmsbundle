<?php

namespace Trendix\CmsBundle\Form;

use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Trendix\AdminBundle\Form\ColorType;
use Trendix\AdminBundle\Form\GalleryType;
use Trendix\AdminBundle\Form\HtmlType;
use Trendix\CmsBundle\Entity\Block;
use Trendix\CmsBundle\Entity\ColorBlock;

class CategoryType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, array('label' => 'Título', 'required' => true))
            ->add('slug', TextType::class, array('label' => 'Slug (si está vacío se generará uno)', 'required' => false))
            ->add('description', TextareaType::class, array('label' => 'Descripción', 'required' => false))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Trendix\CmsBundle\Entity\Category'
        ));
    }

    public function getName()
    {
        return 'trendix_cms_category';
    }
}
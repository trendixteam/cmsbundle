<?php

namespace Trendix\CmsBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Trendix\AdminBundle\Form\ColorType;
use Trendix\AdminBundle\Form\GalleryType;
use Trendix\AdminBundle\Form\HtmlType;
use Trendix\CmsBundle\Entity\Block;
use Trendix\CmsBundle\Entity\Category;
use Trendix\CmsBundle\Entity\ColorBlock;

class PageType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, array('label' => 'Título', 'required' => true))
            ->add('slug', TextType::class, array('label' => 'Slug (si está vacío se generará uno)', 'required' => false))
            ->add('metatitle', TextType::class, array('label' => 'Meta Título', 'required' => false))
            ->add('metadesc', TextareaType::class, array('label' => 'Meta Descripción', 'required' => false))
            ->add('metatags', TextType::class, array('label' => 'Meta Etiquetas', 'required' => false))
        ;

        if(!$options['hide_category']) {
            $builder->add('category', EntityType::class, array(
                'label' => 'Categoría',
                'required' => true,
                'class' => Category::class,
                'query_builder' => function ($er) {
                    return $er->createQueryBuilder('c')->where('c.deleted = false');
                },
            ));
        }
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Trendix\CmsBundle\Entity\Page',
            'hide_category' => false
        ));
    }

    public function getName()
    {
        return 'trendix_cms_page';
    }
}
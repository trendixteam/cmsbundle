<?php

namespace Trendix\CmsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Trendix\AdminBundle\Form\ColorType;
use Trendix\CmsBundle\Entity\ColorBlock;

class CustomBlockType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $customBlocks = explode('|', getenv('CMS_BLOCKS'));
        $custom = [];
        foreach($customBlocks as $block) {
            $blockInfo = explode('-', $block);
            $custom[$blockInfo[1]] = $blockInfo[0];
        }
        $builder
            ->add('title', TextType::class, array('label' => 'Título'))
            ->add('position', NumberType::class, array('label' => 'Posición'))
            ->add('customType', ChoiceType::class, array(
                'label' => 'Subtipo', 'required' => false,
                'choices' => $custom
            ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Trendix\CmsBundle\Entity\CustomBlock'
        ));
    }
}
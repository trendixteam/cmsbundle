<?php

namespace Trendix\CmsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Trendix\AdminBundle\Controller\BaseController;
use Trendix\AdminBundle\Entity\Gallery;
use Trendix\AdminBundle\Entity\Image;
use Trendix\CmsBundle\Entity\CallToActionBlock;
use Trendix\CmsBundle\Entity\CheckBlock;
use Trendix\CmsBundle\Entity\CheckListBlock;
use Trendix\CmsBundle\Entity\ColorBlock;
use Trendix\CmsBundle\Entity\ColorsRowBlock;
use Trendix\CmsBundle\Entity\CustomBlock;
use Trendix\CmsBundle\Entity\IconBlock;
use Trendix\CmsBundle\Entity\IconsRowBlock;
use Trendix\CmsBundle\Entity\Page;
use Trendix\CmsBundle\Entity\TabsBlock;
use Trendix\CmsBundle\Entity\TestimonialBlock;
use Trendix\CmsBundle\Entity\TestimonialsRowBlock;
use Trendix\CmsBundle\Entity\TextBlock;
use Trendix\CmsBundle\Entity\TextGalleryBlock;
use Trendix\CmsBundle\Entity\TextImageBlock;
use Trendix\CmsBundle\Form\BlockType;
use Trendix\CmsBundle\Form\CheckBlockType;
use Trendix\CmsBundle\Form\ColorBlockType;
use Trendix\CmsBundle\Form\IconBlockType;
use Trendix\CmsBundle\Form\PageType;
use Trendix\CmsBundle\Form\TestimonialBlockType;

class DefaultController extends BaseController
{
    /**
     * Edits a block
     *
     * @Route("/admin/page/add/", name="cms_add_page")
     * @Route("/admin/page/edit/{id}", name="cms_edit_page")
     */
    public function newPageAction(Request $request, $id = 0)
    {
        $this->initializeEntityManager();
        if($id) {
            $page = $this->em->getRepository('TrendixCmsBundle:Page')->find($id);
        } else {
            $page = new Page();
        }
        $form = $this->createForm(PageType::class, $page, ['container' => $this->container]);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($page);
            $this->em->flush();

            return $this->redirectToRoute('cms_show_page', array('id' => $page->getId()));
        }

        $this->addData(array(
            'form' => $form->createView(),
        ));
        return $this->render('TrendixCmsBundle::edit_page.html.twig', $this->getData());
    }

    /**
     * Shows a page
     *
     * @Route("/admin/page/show/{id}", name="cms_show_page")
     */
    public function showPageAction(Request $request, $id)
    {
        $this->initializeEntityManager();
        $page = $this->em->getRepository('TrendixCmsBundle:Page')->find($id);
        $this->addData('page', $page);
        return $this->render('TrendixCmsBundle::show_page.html.twig', $this->getData());
    }

    /**
     * Edits a block
     *
     * @Route("/admin/block/edit/{type}/{id}/{redirect}/{idRedirect}", name="edit_block")
     */
    public function editBlockAction($id, $type, $redirect, $idRedirect, Request $request)
    {
        $this->initializeEntityManager();
        $block = $this->em->getRepository('TrendixCmsBundle:Block')->find($id);
        $formClass = 'Trendix\CmsBundle\Form\\' . $block->getClass() . 'Type';
        if($type == 'tab') {
            $form = $this->createForm($formClass, $block);
        } else {
            $form = $this->createForm($formClass, $block);
        }
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($block);
            $this->em->flush();

            return $this->redirectToRoute($redirect, array('id' => $idRedirect));
        }

        $this->addData(array(
            'form' => $form->createView(),
        ));
        return $this->render('TrendixCmsBundle::edit_block.html.twig', $this->getData());
    }

    /**
     * Adds a block to a page
     *
     * @Route("/admin/block/add/block/{id}/{redirect}/{idRedirect}", name="add_block_page")
     */
    public function addBlockAction($id, $redirect, $idRedirect, Request $request)
    {
        $this->initializeEntityManager();
        $form = $this->createForm(BlockType::class);
        $form->handleRequest($request);
        $parent = $this->em->getRepository('TrendixCmsBundle:Page')->find($id);

        return $this->checkForm($redirect, $idRedirect, $request, $form, $parent);
    }

    /**
     * Removes a block
     *
     * @Route("/admin/block/remove/block/{idParent}/{id}/{redirect}/{idRedirect}", name="remove_block")
     */
    public function removeBlockAction($id, $idParent, $redirect, $idRedirect)
    {
        $this->initializeEntityManager();
        $block = $this->em->getRepository('TrendixCmsBundle:Block')->find($id);
        $page = $this->em->getRepository('TrendixCmsBundle:Page')->find($idParent);
        $page->removeBlock($block);
        $this->em->persist($page);
        $this->em->remove($block);
        $this->em->flush();
        return $this->redirectToRoute($redirect, array('id' => $idRedirect));
    }

    /**
     * Adds a color block
     *
     * @Route("/admin/block/add/color/{id}/{redirect}/{idRedirect}", name="add_color_block")
     */
    public function addColorBlockAction($id, $redirect, $idRedirect, Request $request)
    {
        $this->initializeEntityManager();
        $block = new ColorBlock();
        $form = $this->createForm(ColorBlockType::class, $block);
        $form->handleRequest($request);
        $parent = $this->em->getRepository('TrendixCmsBundle:ColorsRowBlock')->find($id);

        if ($form->isSubmitted() && $form->isValid()) {
            $block->setRow($parent);
            $this->em->persist($block);
            $this->em->flush();
            $parent->addColorBlock($block);
            $this->em->persist($parent);
            $this->em->flush();

            return $this->redirectToRoute($redirect, array('id' => $idRedirect));
        }

        $this->addData(array(
            'block' => $block,
            'form' => $form->createView(),
        ));
        return $this->render('TrendixCmsBundle::new_color.html.twig', $this->getData());
    }

    /**
     * Adds a color block
     *
     * @Route("/admin/block/edit-block/color/{id}/{redirect}/{idRedirect}", name="edit_color_block")
     */
    public function editColorBlockAction($id, $redirect, $idRedirect, Request $request)
    {
        $this->initializeEntityManager();
        $block = $this->em->getRepository('TrendixCmsBundle:ColorBlock')->find($id);
        $form = $this->createForm(ColorBlockType::class, $block);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($block);
            $this->em->flush();

            return $this->redirectToRoute($redirect, array('id' => $idRedirect));
        }

        $this->addData(array(
            'block' => $block,
            'form' => $form->createView(),
        ));
        return $this->render('TrendixCmsBundle::new_color.html.twig', $this->getData());
    }

    /**
     * Adds a color block
     *
     * @Route("/admin/block/remove/color/{id}/{redirect}/{idRedirect}", name="remove_color_block")
     */
    public function removeColorBlockAction($id, $redirect, $idRedirect, Request $request)
    {
        $this->initializeEntityManager();
        $block = $this->em->getRepository('TrendixCmsBundle:ColorBlock')->find($id);
        $this->em->remove($block);
        $this->em->flush();
        return $this->redirectToRoute($redirect, array('id' => $idRedirect));
    }

    protected function getUploadsDir()
    {
        $dir = __DIR__  . '/../../../../public/uploads/';
        if(!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }
        return $dir;
    }

    private function getUploadedImage($file)
    {
        if ($file) {
            $fileName = 'logo' . uniqid() . '.' . $file->guessExtension();

            // moves the file to the directory where brochures are stored
            $file->move(
                $this->getUploadsDir(),
                $fileName
            );
        }
        return '/uploads/'. $fileName;
    }

    /**
     * Adds a color block
     *
     * @Route("/admin/block/add/icon/{id}/{redirect}/{idRedirect}", name="add_icon_block")
     */
    public function addIconBlockAction($id, $redirect, $idRedirect, Request $request)
    {
        $this->initializeEntityManager();
        $block = new IconBlock();
        $form = $this->createForm(IconBlockType::class, $block);
        $form->handleRequest($request);
        $parent = $this->em->getRepository('TrendixCmsBundle:IconsRowBlock')->find($id);

        if ($form->isSubmitted() && $form->isValid()) {
            $block->setRow($parent);
            $file = $form->get('icon_file')->getData();
            if($file) {
                $block->setIcon($this->getUploadedImage($file));
            }
            $this->em->persist($block);
            $this->em->flush();
            $parent->addIconBlock($block);
            $this->em->persist($parent);
            $this->em->flush();

            return $this->redirectToRoute($redirect, array('id' => $idRedirect));
        }

        $this->addData(array(
            'block' => $block,
            'form' => $form->createView(),
        ));
        return $this->render('TrendixCmsBundle::new_icon.html.twig', $this->getData());
    }

    /**
     * Adds a color block
     *
     * @Route("/admin/block/edit-block/icon/{id}/{redirect}/{idRedirect}", name="edit_icon_block")
     */
    public function editIconBlockAction($id, $redirect, $idRedirect, Request $request)
    {
        $this->initializeEntityManager();
        $block = $this->em->getRepository('TrendixCmsBundle:IconBlock')->find($id);
        $form = $this->createForm(IconBlockType::class, $block);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $form->get('icon_file')->getData();
            if($file) {
                $block->setIcon($this->getUploadedImage($file));
            }
            $this->em->persist($block);
            $this->em->flush();

            return $this->redirectToRoute($redirect, array('id' => $idRedirect));
        }

        $this->addData(array(
            'block' => $block,
            'form' => $form->createView(),
        ));
        return $this->render('TrendixCmsBundle::new_icon.html.twig', $this->getData());
    }

    /**
     * Adds a color block
     *
     * @Route("/admin/block/remove/icon/{id}/{redirect}/{idRedirect}", name="remove_icon_block")
     */
    public function removeIconBlockAction($id, $redirect, $idRedirect, Request $request)
    {
        $this->initializeEntityManager();
        $block = $this->em->getRepository('TrendixCmsBundle:IconBlock')->find($id);
        $this->em->remove($block);
        $this->em->flush();
        return $this->redirectToRoute($redirect, array('id' => $idRedirect));
    }

    /**
     * Adds a color block
     *
     * @Route("/admin/block/add/testimonial/{id}/{redirect}/{idRedirect}", name="add_testimonial_block")
     */
    public function addTestimonialBlockAction($id, $redirect, $idRedirect, Request $request)
    {
        $this->initializeEntityManager();
        $block = new TestimonialBlock();
        $form = $this->createForm(TestimonialBlockType::class, $block);
        $form->handleRequest($request);
        $parent = $this->em->getRepository('TrendixCmsBundle:TestimonialsRowBlock')->find($id);

        if ($form->isSubmitted() && $form->isValid()) {
            $block->setRow($parent);
            $this->em->persist($block);
            $this->em->flush();
            $parent->addTestimonialBlock($block);
            $this->em->persist($parent);
            $this->em->flush();

            return $this->redirectToRoute($redirect, array('id' => $idRedirect));
        }

        $this->addData(array(
            'block' => $block,
            'form' => $form->createView(),
        ));
        return $this->render('TrendixCmsBundle::new_testimonial.html.twig', $this->getData());
    }

    /**
     * Adds a color block
     *
     * @Route("/admin/block/edit-block/testimonial/{id}/{redirect}/{idRedirect}", name="edit_testimonial_block")
     */
    public function editTestimonialBlockAction($id, $redirect, $idRedirect, Request $request)
    {
        $this->initializeEntityManager();
        $block = $this->em->getRepository('TrendixCmsBundle:TestimonialBlock')->find($id);
        $form = $this->createForm(TestimonialBlockType::class, $block);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($block);
            $this->em->flush();

            return $this->redirectToRoute($redirect, array('id' => $idRedirect));
        }

        $this->addData(array(
            'block' => $block,
            'form' => $form->createView(),
        ));
        return $this->render('TrendixCmsBundle::new_testimonial.html.twig', $this->getData());
    }

    /**
     * Adds a color block
     *
     * @Route("/admin/block/remove/testimonial/{id}/{redirect}/{idRedirect}", name="remove_testimonial_block")
     */
    public function removeTestimonialBlockAction($id, $redirect, $idRedirect, Request $request)
    {
        $this->initializeEntityManager();
        $block = $this->em->getRepository('TrendixCmsBundle:TestimonialBlock')->find($id);
        $this->em->remove($block);
        $this->em->flush();
        return $this->redirectToRoute($redirect, array('id' => $idRedirect));
    }

    /**
     * Adds a color block
     *
     * @Route("/admin/block/add/check/{id}/{redirect}/{idRedirect}", name="add_check_block")
     */
    public function addCheckBlockAction($id, $redirect, $idRedirect, Request $request)
    {
        $this->initializeEntityManager();
        $block = new CheckBlock();
        $form = $this->createForm(CheckBlockType::class, $block);
        $form->handleRequest($request);
        $parent = $this->em->getRepository('TrendixCmsBundle:CheckListBlock')->find($id);

        if ($form->isSubmitted() && $form->isValid()) {
            $block->setRow($parent);
            $this->em->persist($block);
            $this->em->flush();
            $parent->addCheckBlock($block);
            $this->em->persist($parent);
            $this->em->flush();

            return $this->redirectToRoute($redirect, array('id' => $idRedirect));
        }

        $this->addData(array(
            'block' => $block,
            'form' => $form->createView(),
        ));
        return $this->render('TrendixCmsBundle::new_check.html.twig', $this->getData());
    }

    /**
     * Adds a color block
     *
     * @Route("/admin/block/edit-block/check/{id}/{redirect}/{idRedirect}", name="edit_check_block")
     */
    public function editCheckBlockAction($id, $redirect, $idRedirect, Request $request)
    {
        $this->initializeEntityManager();
        $block = $this->em->getRepository('TrendixCmsBundle:CheckBlock')->find($id);
        $form = $this->createForm(CheckBlockType::class, $block);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($block);
            $this->em->flush();

            return $this->redirectToRoute($redirect, array('id' => $idRedirect));
        }

        $this->addData(array(
            'block' => $block,
            'form' => $form->createView(),
        ));
        return $this->render('TrendixCmsBundle::new_check.html.twig', $this->getData());
    }

    /**
     * Adds a color block
     *
     * @Route("/admin/block/remove/check/{id}/{redirect}/{idRedirect}", name="remove_check_block")
     */
    public function removeCheckBlockAction($id, $redirect, $idRedirect, Request $request)
    {
        $this->initializeEntityManager();
        $block = $this->em->getRepository('TrendixCmsBundle:CheckBlock')->find($id);
        $this->em->remove($block);
        $this->em->flush();
        return $this->redirectToRoute($redirect, array('id' => $idRedirect));
    }

    /**
     * Adds a tab block
     *
     * @Route("/admin/block/add/tab/{id}/{redirect}/{idRedirect}", name="add_tab_block")
     */
    public function addTabBlockAction($id, $redirect, $idRedirect, Request $request)
    {

        $this->initializeEntityManager();
        $form = $this->createForm(BlockType::class, null, ['subBlock' => true]);
        $form->handleRequest($request);
        $parent = $this->em->getRepository('TrendixCmsBundle:TabsBlock')->find($id);

        return $this->checkForm($redirect, $idRedirect, $request, $form, $parent);
    }

    /**
     * Removes a tab
     *
     * @Route("/admin/block/remove/tab/{idParent}/{id}/{redirect}/{idRedirect}", name="remove_tab")
     */
    public function removeTabBlockAction($id, $idParent, $redirect, $idRedirect)
    {
        $this->initializeEntityManager();
        $block = $this->em->getRepository('TrendixCmsBundle:Block')->find($id);
        $page = $this->em->getRepository('TrendixCmsBundle:TabsBlock')->find($idParent);
        $page->removeBlock($block);
        $this->em->persist($page);
        $this->em->remove($block);
        $this->em->flush();
        return $this->redirectToRoute($redirect, array('id' => $idRedirect));
    }

    /**
     * @param $redirect
     * @param $idRedirect
     * @param Request $request
     * @param $form
     * @param $parent
     * @param null $block
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    private function checkForm($redirect, $idRedirect, Request $request, $form, $parent, $block = null)
    {
        if ($form->isSubmitted() && $form->isValid()) {
            $formData = $request->request->all();
            $bl = $formData['block'];
            $order = intval($bl['position']);
            if ($bl['type'] == 'text') {
                if(!$block) {
                    $block = new TextBlock();
                }
                $block->setTitle($bl['title']);
                $block->setText($bl['text']);
                $block->setPosition($order);
            } elseif ($bl['type'] == 't_gallery') {
                if(!$block) {
                    $block = new TextGalleryBlock();
                }
                $block->setTitle($bl['title']);
                $block->setText($bl['text']);
                $gallery = new Gallery();
                $gallery->setGallery($bl['gallery']['gallery']);
                $this->em->persist($gallery);
                $this->em->flush();

                $block->setGallery($gallery);
                $block->setGalleryLeft($bl['galleryLeft']);
                $block->setPosition($order);
            } elseif ($bl['type'] == 't_image') {
                if(!$block) {
                    $block = new TextImageBlock();
                }
                $block->setTitle($bl['title']);
                $block->setText($bl['text']);
                $image = new Image();
                $image->setImage($bl['image']['image']);
                $this->em->persist($image);
                $this->em->flush();

                $block->setImage($image);
                $block->setGalleryLeft($bl['galleryLeft']);
                $block->setPosition($order);
            } elseif ($bl['type'] == 'colors') {
                if(!$block) {
                    $block = new ColorsRowBlock();
                }
                $block->setTitle($bl['title']);
                $block->setPosition($order);
                $block->setBlocksPerRow(intval($bl['blocksPerRow']));
            } elseif ($bl['type'] == 'tabs') {
                if(!$block) {
                    $block = new TabsBlock();
                }
                $block->setTitle($bl['title']);
                $block->setPosition($order);
            } elseif ($bl['type'] == 'testimonials') {
                if(!$block) {
                    $block = new TestimonialsRowBlock();
                }
                $block->setTitle($bl['title']);
                $block->setPosition($order);
            } elseif ($bl['type'] == 'checklist') {
                if(!$block) {
                    $block = new CheckListBlock();
                }
                $block->setTitle($bl['title']);
                $block->setPosition($order);
            } elseif ($bl['type'] == 'custom') {
                if(!$block) {
                    $block = new CustomBlock();
                }
                $block->setTitle($bl['title']);
                $block->setCustomType($bl['customType']);
                $block->setPosition($order);
            } elseif ($bl['type'] == 'cta') {
                if(!$block) {
                    $block = new CallToActionBlock();
                }
                $block->setTitle($bl['title']);
                $block->setLink($bl['link']);
                $block->setLinkText($bl['linkText']);
                $block->setColor($bl['color']);
                $block->setBackgroundColor($bl['backgroundColor']);
                $image = new Image();
                $image->setImage($bl['image']['image']);
                $this->em->persist($image);
                $this->em->flush();
                $block->setImage($image);
                $block->setPosition($order);
            } elseif ($bl['type'] == 'icons') {
                if(!$block) {
                    $block = new IconsRowBlock();
                }
                $block->setTitle($bl['title']);
                $block->setPosition($order);
                $block->setBlocksPerRow(intval($bl['blocksPerRow']));
                if(isset($bl['withLinks'])) {
                    $block->setWithLinks($bl['withLinks']);
                }
            }
            if (isset($block)) {
                $this->em->persist($block);
                $this->em->flush();
                $parent->addBlock($block);
            }
            if($parent) {
                $this->em->persist($parent);
                $this->em->flush();
            }
            return $this->redirectToRoute($redirect, array('id' => $idRedirect));
        }

        $this->addData(array(
            'form' => $form->createView(),
        ));
        return $this->render('TrendixCmsBundle::new_block.html.twig', $this->getData());
    }
}

<?php

namespace Trendix\CmsBundle\Controller;

use Trendix\AdminBundle\Utility\StringHelper;
use Trendix\CmsBundle\Lista\CategoryList;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Trendix\AdminBundle\Controller\BaseController;
use Trendix\CmsBundle\Entity\Category;
use Trendix\CmsBundle\Form\CategoryType;

class CategoryController extends BaseController
{
    /**
     * Categories list
     *
     * @Route("/admin/cms/categories/", name="cms_list_categories")
     */
    public function listAction(Request $request)
    {
        $this->initializeEntityManager();
        return $this->generateListWithRenderView(new CategoryList(), "Lista de categorías");
    }

    /**
     * Edits a block
     *
     * @Route("/admin/cms/category/show/{id}", name="cms_show_category")
     */
    public function showCategoryAction(Request $request, $id)
    {
        $this->initializeEntityManager();
        $category = $this->em->getRepository('TrendixCmsBundle:Category')->find($id);
        $this->addData('category', $category);
        return $this->render('TrendixCmsBundle:category:show.html.twig', $this->getData());
    }

    /**
     * Edits a block
     *
     * @Route("/admin/cms/category/edit/{id}", name="cms_edit_category")
     * @Route("/admin/cms/category/add/", name="cms_add_category")
     */
    public function editCategoryAction(Request $request, $id = 0)
    {
        $this->initializeEntityManager();
        if($id) {
            $category = $this->em->getRepository('TrendixCmsBundle:Category')->find($id);
        } else {
            $category = new Category();
        }
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            if(!$category->getSlug()) {
                $category->setSlug(StringHelper::slugify($category->getTitle()));
            }
            $this->em->persist($category);
            $this->em->flush();

            return $this->redirectToRoute('cms_show_category', array('id' => $category->getId()));
        }

        $this->addData(array(
            'form' => $form->createView(),
        ));
        return $this->render('TrendixCmsBundle:category:edit.html.twig', $this->getData());
    }

    /**
     * Removes a block
     *
     * @Route("/admin/cms/category/delete/{id}/", name="cms_delete_category")
     */
    public function removeCategoryAction($id)
    {
        $this->initializeEntityManager();
        $category = $this->em->getRepository('TrendixCmsBundle:Category')->find($id);
        $category->setDeleted(true);
        $this->em->persist($category);
        $this->em->flush();
        return $this->redirectToRoute('cms_list_categories');
    }
}

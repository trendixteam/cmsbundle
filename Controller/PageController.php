<?php

namespace Trendix\CmsBundle\Controller;

use Trendix\AdminBundle\Utility\StringHelper;
use Trendix\CmsBundle\Lista\PageList;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Trendix\AdminBundle\Controller\BaseController;
use Trendix\AdminBundle\Entity\Gallery;
use Trendix\CmsBundle\Entity\ColorBlock;
use Trendix\CmsBundle\Entity\ColorsRowBlock;
use Trendix\CmsBundle\Entity\Page;
use Trendix\CmsBundle\Entity\TabsBlock;
use Trendix\CmsBundle\Entity\TextBlock;
use Trendix\CmsBundle\Entity\TextGalleryBlock;
use Trendix\CmsBundle\Form\BlockType;
use Trendix\CmsBundle\Form\ColorBlockType;
use Trendix\CmsBundle\Form\PageType;

class PageController extends BaseController
{
    /**
     * Pages list
     *
     * @Route("/admin/cms/pages/{id}", name="cms_category_pages")
     * @Route("/admin/cms/pages/", name="cms_list_pages")
     */
    public function listAction(Request $request, $id = 0)
    {
        $this->initializeEntityManager();
        if($id) {
            $category = $this->em->getRepository('TrendixCmsBundle:Category')->find($id);
            return $this->generateListWithRenderView(new PageList($id), "Páginas de {$category->getTitle()}");
        }
        return $this->generateListWithRenderView(new PageList(), "Lista de páginas");
    }

    /**
     * Edits a block
     *
     * @Route("/admin/cms/page/show/{id}", name="cms_show_page")
     */
    public function showPageAction(Request $request, $id)
    {
        $this->initializeEntityManager();
        $page = $this->em->getRepository('TrendixCmsBundle:Page')->find($id);
        $this->addData('page', $page);

        return $this->render('TrendixCmsBundle:page:show.html.twig', $this->getData());
    }

    /**
     * Edits a block
     *
     * @Route("/admin/cms/page/edit/{id}", name="cms_edit_page")
     * @Route("/admin/cms/page/add/", name="cms_add_page")
     */
    public function editPageAction(Request $request, $id = 0)
    {
        $this->initializeEntityManager();
        $category = isset($_GET['category']) ? $_GET['category'] : null;
        if($id) {
            $page = $this->em->getRepository('TrendixCmsBundle:Page')->find($id);
            $category = $page->getCategory();
        } else {
            $page = new Page();
            if($category) {
                $category = $this->em->getRepository('TrendixCmsBundle:Category')->find($category);
                $page->setCategory($category);
            }
        }
        $form = $this->createForm(PageType::class, $page);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            if(!$page->getSlug()) {
                $page->setSlug(StringHelper::slugify($page->getTitle()));
            }
            $this->em->persist($page);
            $this->em->flush();

            return $this->redirectToRoute('cms_show_page', array('id' => $page->getId()));
        }

        $this->addData(array(
            'form' => $form->createView(),
            'category' => $category,
            'new' => $id == 0
        ));
        return $this->render('TrendixCmsBundle:page:edit.html.twig', $this->getData());
    }

    /**
     * Removes a block
     *
     * @Route("/admin/cms/page/delete/{id}/", name="cms_delete_page")
     */
    public function removePageAction($id)
    {
        $this->initializeEntityManager();
        $page = $this->em->getRepository('TrendixCmsBundle:Page')->find($i);
        $page->setDeleted(true);
        $this->em->persist($page);
        $this->em->flush();
        return $this->redirectToRoute('cms_list_pages');
    }
}
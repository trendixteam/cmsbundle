<?php

namespace Trendix\CmsBundle\Lista;

use Trendix\AdminBundle\Component\Lista\DataType\JoinType;
use Trendix\AdminBundle\Component\Lista\DataType\TextType;
use Trendix\AdminBundle\Component\Lista\ListaAbstractBuilder;
use Trendix\AdminBundle\Component\Lista\ListaAbstractType;
use Trendix\CmsBundle\Entity\Page;

class PageList extends ListaAbstractType
{
    private $categoryId = null;

    /**
     * PageList constructor.
     * @param null $categoryId
     */
    public function __construct($categoryId = null)
    {
        $this->categoryId = $categoryId;
    }

    public function buildList(ListaAbstractBuilder $builder, array $options)
    {
        $builder
            ->add('id')
            ->add('title', TextType::class, ['label' => 'Título'])
            ->add('slug', TextType::class, ['label' => 'Slug'])
            ->add('category.title', JoinType::class, ['label' => 'Categoría'])
            ->addAction('edit', 'edit', [
                'route' => 'cms_edit_page',
                'route_list_params' => ['id' => 'id']
            ])
            ->addAction('show', 'eye', [
                'route' => 'cms_show_page',
                'route_list_params' => ['id' => 'id']
            ])
            ->addAction('delete', 'delete', [
                'route' => 'cms_delete_page',
                'route_list_params' => ['id' => 'id']
            ])
            ->addAction('primary', 'plus', [
                'route' => 'cms_add_page',
                'route_list_params' => ['category' => $this->categoryId]
            ]);
    }

    public function getClass()
    {
        return Page::class;
    }

    public function getOptions()
    {
        $addWhere = '';

        if($this->categoryId) {
            $addWhere = ' AND a1.id = ' . $this->categoryId;
        }

        return [
            'permanent_filter' => 'a. deleted = false' . $addWhere,
            'specific_search_fields' => [
                'category.slug' => [
                    'value' => '',
                    'type' => 'text',
                    'label' => 'slug categoría'
                ],
                'id' => [
                    'value' => '',
                    'type' => 'number',
                    'label' => 'id'
                ],
                'category.id' => [
                    'value' => '',
                    'type' => 'choice',
                    'label' => 'Categoría',
                    'choices' => [
                        ['value' => '1', 'label' => 'Categoría 1'],
                        ['value' => '2', 'label' => 'test-2']
                    ]
                ]
            ]
        ];
    }
}

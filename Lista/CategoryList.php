<?php

namespace Trendix\CmsBundle\Lista;

use Trendix\AdminBundle\Component\Lista\DataType\JoinType;
use Trendix\AdminBundle\Component\Lista\DataType\TextType;
use Trendix\AdminBundle\Component\Lista\ListaAbstractBuilder;
use Trendix\AdminBundle\Component\Lista\ListaAbstractType;
use Trendix\CmsBundle\Entity\Category;

class CategoryList extends ListaAbstractType
{
    public function buildList(ListaAbstractBuilder $builder, array $options)
    {
        $builder
            ->add('id')
            ->add('title', TextType::class, ['label' => 'Título'])
            ->add('slug', TextType::class, ['label' => 'Slug'])
            ->addAction('edit', 'edit', [
                'route' => 'cms_edit_category',
                'route_list_params' => ['id' => 'id']
            ])
            ->addAction('show', 'eye', [
                'route' => 'cms_show_category',
                'route_list_params' => ['id' => 'id']
            ])
            ->addAction('list', 'format-list-bulleted', [
                'route' => 'cms_category_pages',
                'route_list_params' => ['id' => 'id']
            ])
            ->addAction('delete', 'delete', [
                'route' => 'cms_delete_category',
                'route_list_params' => ['id' => 'id']
            ])
            ->addAction('primary', 'plus', [
                'route' => 'cms_add_category'
            ]);
    }

    public function getClass()
    {
        return Category::class;
    }

    public function getOptions()
    {
        return ['permanent_filter' => 'a. deleted = false'];
    }
}

<?php

namespace Trendix\CmsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * CheckListBlock
 *
 * @ORM\Entity(repositoryClass="Trendix\CmsBundle\Repository\CustomBlockRepository")
 */
class CustomBlock extends Block
{

    /**
     * @var string
     *
     * @ORM\Column(name="custom_type", type="string", length=255)
     */
    private $customType;

    public function __construct() {
    }

    /**
     * @return string
     */
    public function getCustomType(): ?string
    {
        return $this->customType;
    }

    /**
     * @param string $customType
     * @return CustomBlock
     */
    public function setCustomType(?string $customType): CustomBlock
    {
        $this->customType = $customType;
        return $this;
    }

}


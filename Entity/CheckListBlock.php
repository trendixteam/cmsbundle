<?php

namespace Trendix\CmsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * CheckListBlock
 *
 * @ORM\Entity(repositoryClass="Trendix\CmsBundle\Repository\CheckListBlockRepository")
 */
class CheckListBlock extends Block
{
    /**
     * @ORM\OneToMany(targetEntity="CheckBlock", mappedBy="row", cascade={"persist"})
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $checkBlocks;

    public function __construct() {
        $this->checkBlocks = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getCheckBlocks()
    {
        return $this->checkBlocks;
    }

    /**
     * @param mixed $checkBlocks
     * @return CheckListBlock
     */
    public function setCheckBlocks($checkBlocks)
    {
        $this->checkBlocks = $checkBlocks;
        return $this;
    }

    /**
     * @param mixed $checkBlock
     * @return CheckListBlock
     */
    public function addCheckBlock($checkBlock)
    {
        $this->checkBlocks->add($checkBlock);
        return $this;
    }

    /**
     * @param mixed $checkBlock
     * @return CheckListBlock
     */
    public function removeCheckBlock($checkBlock)
    {
        if($this->checkBlocks->contains($checkBlock)) {
            $this->checkBlocks->removeElement($checkBlock);
        }
        return $this;
    }
}


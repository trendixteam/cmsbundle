<?php

namespace Trendix\CmsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * TestimonialsRowBlock
 *
 * @ORM\Entity(repositoryClass="Trendix\CmsBundle\Repository\TestimonialsRowBlockRepository")
 */
class TestimonialsRowBlock extends Block
{
    /**
     * @ORM\OneToMany(targetEntity="TestimonialBlock", mappedBy="row", cascade={"persist"})
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $testimonialBlocks;

    public function __construct() {
        $this->testimonialBlocks = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getTestimonialBlocks()
    {
        return $this->testimonialBlocks;
    }

    /**
     * @param mixed $testimonialBlocks
     * @return TestimonialsRowBlock
     */
    public function setTestimonialBlocks($testimonialBlocks)
    {
        $this->testimonialBlocks = $testimonialBlocks;
        return $this;
    }

    /**
     * @param mixed $testimonialBlock
     * @return TestimonialsRowBlock
     */
    public function addTestimonialBlock($testimonialBlock)
    {
        $this->testimonialBlocks->add($testimonialBlock);
        return $this;
    }

    /**
     * @param mixed $testimonialBlock
     * @return TestimonialsRowBlock
     */
    public function removeTestimonialBlock($testimonialBlock)
    {
        if($this->testimonialBlocks->contains($testimonialBlock)) {
            $this->testimonialBlocks->removeElement($testimonialBlock);
        }
        return $this;
    }
}


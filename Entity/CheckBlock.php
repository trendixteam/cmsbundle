<?php

namespace Trendix\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ColorBlock
 *
 * @ORM\Table(name="check_block")
 * @ORM\Entity(repositoryClass="Trendix\CmsBundle\Repository\CheckBlockRepository")
 */
class CheckBlock
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="string", length=255)
     */
    private $text;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer")
     */
    private $position;

    /**
     * @ORM\ManyToOne(targetEntity="CheckListBlock", inversedBy="checkBlocks")
     * @ORM\JoinColumn(name="check_id", referencedColumnName="id")
     */
    private $row;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return ColorBlock
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @return mixed
     */
    public function getRow()
    {
        return $this->row;
    }

    /**
     * @param mixed $row
     * @return ColorBlock
     */
    public function setRow($row)
    {
        $this->row = $row;
        return $this;
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param int $position
     * @return ColorBlock
     */
    public function setPosition($position)
    {
        $this->position = $position;
        return $this;
    }

    function __toString()
    {
        return $this->text;
    }
}


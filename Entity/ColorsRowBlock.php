<?php

namespace Trendix\CmsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * ColorsRowBlock
 *
 * @ORM\Entity(repositoryClass="Trendix\CmsBundle\Repository\ColorsRowBlockRepository")
 */
class ColorsRowBlock extends Block
{
    /**
     * @var int
     *
     * @ORM\Column(name="blocks_per_row", type="integer")
     */
    private $blocksPerRow;

    /**
     * @ORM\OneToMany(targetEntity="ColorBlock", mappedBy="row", cascade={"persist"})
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $colorBlocks;

    public function __construct() {
        $this->colorBlocks = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getColorBlocks()
    {
        return $this->colorBlocks;
    }

    /**
     * @param mixed $colorBlocks
     * @return ColorsRowBlock
     */
    public function setColorBlocks($colorBlocks)
    {
        $this->colorBlocks = $colorBlocks;
        return $this;
    }

    /**
     * @param mixed $colorBlock
     * @return ColorsRowBlock
     */
    public function addColorBlock($colorBlock)
    {
        $this->colorBlocks->add($colorBlock);
        return $this;
    }

    /**
     * @param mixed $colorBlock
     * @return ColorsRowBlock
     */
    public function removeColorBlock($colorBlock)
    {
        if($this->colorBlocks->contains($colorBlock)) {
            $this->colorBlocks->removeElement($colorBlock);
        }
        return $this;
    }

    /**
     * @return int
     */
    public function getBlocksPerRow()
    {
        return $this->blocksPerRow;
    }

    /**
     * @param int $blocksPerRow
     * @return ColorsRowBlock
     */
    public function setBlocksPerRow($blocksPerRow)
    {
        $this->blocksPerRow = $blocksPerRow;
        return $this;
    }

}


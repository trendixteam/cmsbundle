<?php

namespace Trendix\CmsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Trendix\AdminBundle\Utility\StringHelper;

/**
 * Page
 *
 * @ORM\Table(name="page")
 * @ORM\Entity(repositoryClass="Trendix\CmsBundle\Repository\PageRepository")
 */
class Page
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @ORM\ManyToMany(targetEntity="Block")
     * @ORM\JoinTable(name="pages_blocks",
     *      joinColumns={@ORM\JoinColumn(name="page_id", referencedColumnName="id", onDelete="cascade")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="block_id", referencedColumnName="id", onDelete="cascade")}
     *      )
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $blocks;

    /**
     * @var Category
     * @ORM\ManyToOne(targetEntity="Trendix\CmsBundle\Entity\Category", inversedBy="pages")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;

    use SeoPropertiesTrait;
    use SlugPropertyTrait;
    use SoftDeleteTrait;
    use TimeTraceTrait;

    /**
     * Page constructor.
     */
    public function __construct()
    {
        $this->blocks = new ArrayCollection();
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Page
     */
    public function setTitle($title)
    {
        $this->title = $title;
        $this->slug = StringHelper::slugify($title);
        $this->updatedAt = new \DateTime();

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getBlocks()
    {
        return $this->blocks;
    }

    /**
     * @param mixed $blocks
     * @return Page
     */
    public function setBlocks($blocks)
    {
        $this->blocks = $blocks;
        $this->updatedAt = new \DateTime();
        return $this;
    }

    /**
     * @param mixed $block
     * @return Page
     */
    public function addBlock($block)
    {
        $this->blocks->add($block);
        return $this;
    }

    /**
     * @param mixed $block
     * @return Page
     */
    public function removeBlock($block)
    {
        if($this->blocks->contains($block)) {
            $this->blocks->removeElement($block);
        }
        return $this;
    }

    /**
     * @return Category
     */
    public function getCategory(): ?Category
    {
        return $this->category;
    }

    /**
     * @param Category $category
     * @return Page
     */
    public function setCategory(Category $category): Page
    {
        $this->category = $category;
        return $this;
    }

    public function __toString()
    {
        return $this->title ? $this->title : '';
    }

    public function update()
    {
        foreach ($this->blocks as $block) {
            if($block->getUpdatedAt() > $this->updatedAt) {
                $this->updatedAt = $block->getUpdatedAt();
            }
        }

        return $this;
    }
}


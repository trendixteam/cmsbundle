<?php

namespace Trendix\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TextImageBlock
 *
 * @ORM\Entity(repositoryClass="Trendix\CmsBundle\Repository\TextImageBlockRepository")
 */
class CallToActionBlock extends Block
{

    /**
     * @var string
     *
     * @ORM\Column(name="gallery_text", type="text")
     */
    private $text;

    /**
     * @var string
     *
     * @ORM\Column(name="cta_type", type="string", length=50)
     */
    private $ctaType;

    /**
     * @ORM\ManyToOne(targetEntity="Trendix\AdminBundle\Entity\Image", cascade={"persist"})
     * @ORM\JoinColumn(name="image_id", referencedColumnName="id")
     */
    private $image;

    /**
     * @var string
     * @ORM\Column(name="color", type="string", length=50, nullable=true)
     */
    private $color;

    /**
     * @var string
     * @ORM\Column(name="cta_link", type="string", length=255, nullable=true)
     */
    private $link;

    /**
     * @var string
     * @ORM\Column(name="cta_link_text", type="string", length=255, nullable=true)
     */
    private $linkText;

    /**
     * @var string
     * @ORM\Column(name="background_color", type="string", length=50, nullable=true)
     */
    private $backgroundColor;


    /**
     * Set text
     *
     * @param string $text
     *
     * @return TextImageBlock
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set gallery
     *
     * @param /TrendixAdmin/Image $image
     *
     * @return TextImageBlock
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get gallery
     *
     * @return /TrendixAdmin/Image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @return string
     */
    public function getCtaType(): ?string
    {
        return $this->ctaType;
    }

    /**
     * @param string $ctaType
     * @return CallToActionBlock
     */
    public function setCtaType(?string $ctaType): CallToActionBlock
    {
        $this->ctaType = $ctaType;
        return $this;
    }

    /**
     * @return string
     */
    public function getColor(): ?string
    {
        return $this->color;
    }

    /**
     * @param string $color
     * @return CallToActionBlock
     */
    public function setColor(?string $color): CallToActionBlock
    {
        $this->color = $color;
        return $this;
    }

    /**
     * @return string
     */
    public function getBackgroundColor(): ?string
    {
        return $this->backgroundColor;
    }

    /**
     * @param string $backgroundColor
     * @return CallToActionBlock
     */
    public function setBackgroundColor(?string $backgroundColor): CallToActionBlock
    {
        $this->backgroundColor = $backgroundColor;
        return $this;
    }

    /**
     * @return string
     */
    public function getLink(): ?string
    {
        return $this->link;
    }

    /**
     * @param string $link
     * @return CallToActionBlock
     */
    public function setLink(?string $link): CallToActionBlock
    {
        $this->link = $link;
        return $this;
    }

    /**
     * @return string
     */
    public function getLinkText(): ?string
    {
        return $this->linkText;
    }

    /**
     * @param string $linkText
     * @return CallToActionBlock
     */
    public function setLinkText(string $linkText): CallToActionBlock
    {
        $this->linkText = $linkText;
        return $this;
    }

}


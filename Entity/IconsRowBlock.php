<?php

namespace Trendix\CmsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * ColorsRowBlock
 *
 * @ORM\Entity(repositoryClass="Trendix\CmsBundle\Repository\IconsRowBlockRepository")
 */
class IconsRowBlock extends Block
{
    /**
     * @var int
     *
     * @ORM\Column(name="blocks_per_row", type="integer")
     */
    private $blocksPerRow;

    /**
     * @var boolean
     *
     * @ORM\Column(name="with_links", type="boolean")
     */
    private $withLinks = false;

    /**
     * @ORM\OneToMany(targetEntity="IconBlock", mappedBy="row", cascade={"persist"})
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $iconBlocks;

    public function __construct() {
        $this->colorBlocks = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getIconBlocks()
    {
        return $this->iconBlocks;
    }

    /**
     * @param mixed $iconBlocks
     * @return ColorsRowBlock
     */
    public function setIconBlocks($iconBlocks)
    {
        $this->iconBlocks = $iconBlocks;
        return $this;
    }

    /**
     * @param mixed $colorBlock
     * @return ColorsRowBlock
     */
    public function addIconBlock($colorBlock)
    {
        $this->iconBlocks->add($colorBlock);
        return $this;
    }

    /**
     * @param mixed $colorBlock
     * @return ColorsRowBlock
     */
    public function removeIconBlock($colorBlock)
    {
        if($this->iconBlocks->contains($colorBlock)) {
            $this->iconBlocks->removeElement($colorBlock);
        }
        return $this;
    }

    /**
     * @return int
     */
    public function getBlocksPerRow()
    {
        return $this->blocksPerRow;
    }

    /**
     * @param int $blocksPerRow
     * @return ColorsRowBlock
     */
    public function setBlocksPerRow($blocksPerRow)
    {
        $this->blocksPerRow = $blocksPerRow;
        return $this;
    }

    /**
     * @return bool
     */
    public function isWithLinks(): ?bool
    {
        return $this->withLinks;
    }

    /**
     * @param bool $withLinks
     * @return IconsRowBlock
     */
    public function setWithLinks(bool $withLinks): IconsRowBlock
    {
        $this->withLinks = $withLinks;
        return $this;
    }

    public function __toString()
    {
        return $this->getTitle() ? $this->getTitle() : '';
    }

}


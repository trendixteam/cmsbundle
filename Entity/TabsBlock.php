<?php

namespace Trendix\CmsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * TabsBlock
 *
 * @ORM\Entity(repositoryClass="Trendix\CmsBundle\Repository\TabsBlockRepository")
 */
class TabsBlock extends Block
{

    /**
     * @ORM\ManyToMany(targetEntity="Block")
     * @ORM\JoinTable(name="tabs_block_blocks",
     *      joinColumns={@ORM\JoinColumn(name="tab_block_id", referencedColumnName="id", onDelete="cascade")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="tab_id", referencedColumnName="id", onDelete="cascade")}
     *      )
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $tabs;

    /**
     * Page constructor.
     */
    public function __construct()
    {
        $this->tabs = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getTabs()
    {
        return $this->tabs;
    }

    /**
     * @param mixed $tabs
     * @return TabsBlock
     */
    public function setTabs($tabs)
    {
        $this->tabs = $tabs;
        return $this;
    }

    /**
     * @param mixed $tab
     * @return TabsBlock
     */
    public function addTab($tab)
    {
        $this->tabs->add($tab);
        return $this;
    }

    /**
     * @param mixed $tab
     * @return TabsBlock
     */
    public function removeTab($tab)
    {
        if($this->tabs->contains($tab)) {
            $this->tabs->removeElement($tab);
        }
        return $this;
    }

    /**
     * @param mixed $tab
     * @return TabsBlock
     */
    public function addBlock($tab)
    {
        return $this->addTab($tab);
    }

    /**
     * @param mixed $tab
     * @return TabsBlock
     */
    public function removeBlock($tab)
    {
        return $this->removeTab($tab);
    }
}


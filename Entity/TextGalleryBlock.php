<?php

namespace Trendix\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TextGalleryBlock
 *
 * @ORM\Entity(repositoryClass="Trendix\CmsBundle\Repository\TextGalleryBlockRepository")
 */
class TextGalleryBlock extends Block
{

    /**
     * @var string
     *
     * @ORM\Column(name="gallery_text", type="text")
     */
    private $text;

    /**
     * @var bool
     *
     * @ORM\Column(name="galleryLeft", type="boolean")
     */
    private $galleryLeft;

    /**
     * @ORM\ManyToOne(targetEntity="Trendix\AdminBundle\Entity\Gallery", cascade={"persist"})
     * @ORM\JoinColumn(name="gallery_id", referencedColumnName="id")
     */
    private $gallery;


    /**
     * Set text
     *
     * @param string $text
     *
     * @return TextGalleryBlock
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set galleryLeft
     *
     * @param boolean $galleryLeft
     *
     * @return TextGalleryBlock
     */
    public function setGalleryLeft($galleryLeft)
    {
        $this->galleryLeft = $galleryLeft;

        return $this;
    }

    /**
     * Get galleryLeft
     *
     * @return bool
     */
    public function getGalleryLeft()
    {
        return $this->galleryLeft;
    }

    /**
     * Set gallery
     *
     * @param /TrendixAdmin/Gallery $gallery
     *
     * @return TextGalleryBlock
     */
    public function setGallery($gallery)
    {
        $this->gallery = $gallery;

        return $this;
    }

    /**
     * Get gallery
     *
     * @return /TrendixAdmin/Gallery
     */
    public function getGallery()
    {
        return $this->gallery;
    }
}


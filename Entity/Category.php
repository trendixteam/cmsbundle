<?php

namespace Trendix\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use mysql_xdevapi\Collection;

/**
 * Category
 *
 * @ORM\Table(name="category")
 * @ORM\Entity(repositoryClass="Trendix\CmsBundle\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="title", type="text")
     */
    private $title;

    /**
     * @var string
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var Page
     * One product has many features. This is the inverse side.
     * @ORM\OneToMany(targetEntity="Trendix\CmsBundle\Entity\Page", mappedBy="category")
     */
    private $pages;

    use SlugPropertyTrait;
    use SoftDeleteTrait;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Category
     */
    public function setTitle(string $title): Category
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Category
     */
    public function setDescription(string $description): Category
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getPages()
    {
        return $this->pages;
    }

    /**
     * @param Collection $pages
     * @return Category
     */
    public function setPages($pages)
    {
        $this->pages = $pages;
        return $this;
    }

    public function __toString()
    {
        return $this->title ? $this->title : '';
    }
}
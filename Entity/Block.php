<?php

namespace Trendix\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Block
 *
 * @ORM\Table(name="block")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({"block" = "Block", "t_gallery" = "TextGalleryBlock", "t_image" = "TextImageBlock", "colors" = "ColorsRowBlock",
 *                      "tabs" = "TabsBlock", "text" = "TextBlock", "icons" = "IconsRowBlock", "testimonials" = "TestimonialsRowBlock",
*                       "cta" = "CallToActionBlock", "checklist" = "CheckListBlock", "custom" = "CustomBlock"})
 * @ORM\Entity(repositoryClass="Trendix\CmsBundle\Repository\BlockRepository")
 */
class Block
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer")
     */
    private $position;

    use TimeTraceTrait;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Block
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param integer $position
     * @return Block
     */
    public function setPosition($position)
    {
        $this->position = $position;
        return $this;
    }

    function __toString()
    {
        return $this->title ? (string)$this->title : ' ';
    }

    public function getType()
    {
        $table = array(
            "t_image" => "TextImageBlock",
            "t_gallery" => "TextGalleryBlock",
            "colors" => "ColorsRowBlock",
            "testimonials" => "TestimonialsRowBlock",
            "checklist" => "CheckListBlock",
            "icons" => "IconsRowBlock",
            "tabs" => "TabsBlock",
            "text" => "TextBlock",
            "cta" => "CallToActionBlock",
            "custom" => "CustomBlock"
        );
        return array_search((new \ReflectionClass($this))->getShortName(), $table);
    }

    public function getClassPath()
    {
        return (new \ReflectionClass($this))->getName();
    }

    public function getClass()
    {
        return (new \ReflectionClass($this))->getShortName();
    }

}


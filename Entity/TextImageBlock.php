<?php

namespace Trendix\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TextImageBlock
 *
 * @ORM\Entity(repositoryClass="Trendix\CmsBundle\Repository\TextImageBlockRepository")
 */
class TextImageBlock extends Block
{

    /**
     * @var string
     *
     * @ORM\Column(name="gallery_text", type="text")
     */
    private $text;

    /**
     * @var bool
     *
     * @ORM\Column(name="galleryLeft", type="boolean")
     */
    private $galleryLeft;

    /**
     * @ORM\ManyToOne(targetEntity="Trendix\AdminBundle\Entity\Image", cascade={"persist"})
     * @ORM\JoinColumn(name="image_id", referencedColumnName="id")
     */
    private $image;


    /**
     * Set text
     *
     * @param string $text
     *
     * @return TextImageBlock
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set galleryLeft
     *
     * @param boolean $galleryLeft
     *
     * @return TextImageBlock
     */
    public function setGalleryLeft($galleryLeft)
    {
        $this->galleryLeft = $galleryLeft;

        return $this;
    }

    /**
     * Get galleryLeft
     *
     * @return bool
     */
    public function getGalleryLeft()
    {
        return $this->galleryLeft;
    }

    /**
     * Set gallery
     *
     * @param /TrendixAdmin/Image $image
     *
     * @return TextImageBlock
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get gallery
     *
     * @return /TrendixAdmin/Image
     */
    public function getImage()
    {
        return $this->image;
    }
}


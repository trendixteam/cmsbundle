<?php

namespace Trendix\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ColorBlock
 *
 * @ORM\Table(name="testimonial_block")
 * @ORM\Entity(repositoryClass="Trendix\CmsBundle\Repository\TestimonialBlockRepository")
 */
class TestimonialBlock
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="author", type="string", length=255)
     */
    private $author;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="string", length=255)
     */
    private $text;

    /**
     * @var string
     *
     * @ORM\Column(name="place", type="string", length=255, nullable=true)
     */
    private $place;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer")
     */
    private $position;

    /**
     * @ORM\ManyToOne(targetEntity="Trendix\AdminBundle\Entity\Image", cascade={"persist"})
     * @ORM\JoinColumn(name="image_id", referencedColumnName="id")
     */
    private $image;

    /**
     * @ORM\ManyToOne(targetEntity="TestimonialsRowBlock", inversedBy="testimonialBlocks")
     * @ORM\JoinColumn(name="testimonial_id", referencedColumnName="id")
     */
    private $row;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set author
     *
     * @param string $author
     *
     * @return ColorBlock
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return ColorBlock
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set link
     *
     * @param string $place
     *
     * @return ColorBlock
     */
    public function setPlace($place)
    {
        $this->place = $place;

        return $this;
    }

    /**
     * Get link
     *
     * @return string
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * @return mixed
     */
    public function getRow()
    {
        return $this->row;
    }

    /**
     * @param mixed $row
     * @return ColorBlock
     */
    public function setRow($row)
    {
        $this->row = $row;
        return $this;
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param int $position
     * @return ColorBlock
     */
    public function setPosition($position)
    {
        $this->position = $position;
        return $this;
    }

    function __toString()
    {
        return $this->text;
    }

    /**
     * Set gallery
     *
     * @param /TrendixAdmin/Image $image
     *
     * @return TextImageBlock
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get gallery
     *
     * @return /TrendixAdmin/Image
     */
    public function getImage()
    {
        return $this->image;
    }

}


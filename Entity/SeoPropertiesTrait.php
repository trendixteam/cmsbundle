<?php

namespace Trendix\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


trait SeoPropertiesTrait
{

    /**
     * @var string
     * @ORM\Column(type="string", length=160, nullable=true)
     */
    private $metadesc;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $metatags;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $metatitle;

    public function getMetadesc(): ?string
    {
        return $this->metadesc;
    }

    public function setMetadesc(?string $metadesc): self
    {
        $this->metadesc = $metadesc;

        return $this;
    }

    public function getMetatags(): ?string
    {
        return $this->metatags;
    }

    public function setMetatags(?string $metatags): self
    {
        $this->metatags = $metatags;

        return $this;
    }

    public function getMetatitle(): ?string
    {
        return $this->metatitle;
    }

    public function setMetatitle(?string $metatitle): self
    {
        $this->metatitle = $metatitle;

        return $this;
    }
}
<?php

namespace Trendix\CmsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * TextBlock
 *
 * @ORM\Entity(repositoryClass="Trendix\CmsBundle\Repository\TextBlockRepository")
 */
class TextBlock extends Block
{

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text", nullable=true)
     */
    private $text;

    /**
     * Set text
     *
     * @param string $text
     *
     * @return TextGalleryBlock
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    public function __toString()
    {
        return $this->getTitle() ? (string)$this->getTitle() : ' ';
    }


}


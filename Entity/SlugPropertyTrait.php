<?php

namespace Trendix\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

trait SlugPropertyTrait
{
    /**
     * @var string
     * @ORM\Column(name="slug", type="text")
     */
    private $slug;
    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     * @return SeoPropertiesTrait
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }
}
